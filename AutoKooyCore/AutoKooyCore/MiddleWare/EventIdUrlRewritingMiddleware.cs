﻿using FusionCore.Modules.FundsManager.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoKooyCore.MiddleWare
{
    public class EventIdUrlRewritingMiddleware
    {
        private readonly RequestDelegate _next;

        //Your constructor will have the dependencies needed for database access
        public EventIdUrlRewritingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            ILogger _logger = (ILogger<EventIdUrlRewritingMiddleware>)context.RequestServices.GetService(typeof(ILogger<EventIdUrlRewritingMiddleware>));
            try
            {
                var path = context.Request.Path.ToUriComponent();
                if (!path.StartsWith("/occasions/"))
                {
                    if (PathIsEventId(path, context))
                    {
                        //If is an eventId, change the request path to be "Event/Index/{path}" so it is handled by the event controller, index action
                        context.Request.Path = "/Pagina/Index" + path;
                    }
                }

                //Let the next middleware (MVC routing) handle the request
                //In case the path was updated, the MVC routing will see the updated path
                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                _logger.LogError("EventIdUrlRewritingMiddleware Exception");
                _logger.LogError($"Message : {ex.Message}");
                _logger.LogError($"StackTrace : {ex.StackTrace}");
                if (ex.InnerException != null)
                    _logger.LogError($"InnerException : {ex.InnerException.Message}");
            }
        }

        private bool PathIsEventId(string path, HttpContext httpContext)
        {
            //The real midleware will try to find an event in the database that matches the current path
            //In this example I am just using some hardcoded string
            if (!string.IsNullOrEmpty(path))
            {
                var pageService = (IPageService)httpContext.RequestServices.GetService(typeof(IPageService));
                return pageService.IsExistsURL(Convert.ToString(path));
            }
            return false;
        }
    }
}
