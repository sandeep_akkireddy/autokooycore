﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutoKooyCore.XMLHelpers
{
    public class StripHtml
    {
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
        public static string StripHtmlTags(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        public static string RemoveSpecialcharswithspace(string str)
        {
            try
            {
                str = Regex.Replace(str, "[^0-9a-zA-Z ]+", " ");

                return str.ToLower();
            }
            catch { return str; }
        }
    }

}
