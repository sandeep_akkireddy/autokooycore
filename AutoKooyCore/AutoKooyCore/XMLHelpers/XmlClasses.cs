﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AutoKooyCore.XMLHelpers
{

    [XmlRoot(ElementName = "tellerstand")]
    public class Tellerstand
    {
        [XmlAttribute(AttributeName = "eenheid")]
        public string Eenheid { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "verkoopprijs_particulier")]
    public class Verkoopprijs_particulier
    {
        [XmlAttribute(AttributeName = "bpm")]
        public string Bpm { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "apk")]
    public class Apk
    {
        [XmlAttribute(AttributeName = "tot")]
        public string Tot { get; set; }
        [XmlAttribute(AttributeName = "bij_aflevering")]
        public string Bij_aflevering { get; set; }
    }

    [XmlRoot(ElementName = "bieden_vanaf")]
    public class Bieden_vanaf
    {
        [XmlAttribute(AttributeName = "eenheid")]
        public string Eenheid { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "as")]
    public class As
    {
        [XmlElement(ElementName = "positie")]
        public string Positie { get; set; }
        [XmlElement(ElementName = "merk_type")]
        public string Merk_type { get; set; }
        [XmlElement(ElementName = "max_as_last")]
        public string Max_as_last { get; set; }
        [XmlElement(ElementName = "bandenmaat")]
        public string Bandenmaat { get; set; }
        [XmlElement(ElementName = "aangedreven")]
        public string Aangedreven { get; set; }
        [XmlElement(ElementName = "liftas")]
        public string Liftas { get; set; }
        [XmlElement(ElementName = "dubbellucht")]
        public string Dubbellucht { get; set; }
        [XmlElement(ElementName = "meesturend")]
        public string Meesturend { get; set; }
        [XmlElement(ElementName = "differentieelslot")]
        public string Differentieelslot { get; set; }
        [XmlElement(ElementName = "lichtmetalen_velgen")]
        public string Lichtmetalen_velgen { get; set; }
        [XmlElement(ElementName = "profiel_banden_perc")]
        public string Profiel_banden_perc { get; set; }
        [XmlElement(ElementName = "reductie")]
        public string Reductie { get; set; }
        [XmlElement(ElementName = "remmen")]
        public string Remmen { get; set; }
        [XmlElement(ElementName = "vering")]
        public string Vering { get; set; }
        [XmlAttribute(AttributeName = "nr")]
        public string Nr { get; set; }
    }

    [XmlRoot(ElementName = "asconfiguratie")]
    public class Asconfiguratie
    {
        [XmlElement(ElementName = "as")]
        public List<As> As { get; set; }
    }

    [XmlRoot(ElementName = "wegenbelasting_kwartaal")]
    public class Wegenbelasting_kwartaal
    {
        [XmlAttribute(AttributeName = "min")]
        public string Min { get; set; }
        [XmlAttribute(AttributeName = "max")]
        public string Max { get; set; }
    }

    [XmlRoot(ElementName = "accessoire")]
    public class Accessoire
    {
        [XmlAttribute(AttributeName = "prioriteit")]
        public string _Prioriteit { get; set; }
        [XmlElement(ElementName = "prioriteit")]
        public string Prioriteit { get; set; }
        [XmlText]
        public string Text { get; set; }
        [XmlElement(ElementName = "naam")]
        public string Naam { get; set; }
        [XmlElement(ElementName = "volgorde")]
        public string Volgorde { get; set; }
        [XmlAttribute(AttributeName = "prijs")]
        public string Prijs { get; set; }
    }

    [XmlRoot(ElementName = "pakket")]
    public class Pakket
    {
        [XmlElement(ElementName = "accessoire")]
        public List<Accessoire> Accessoire { get; set; }
        [XmlAttribute(AttributeName = "naam")]
        public string Naam { get; set; }
        [XmlAttribute(AttributeName = "prioriteit")]
        public string Prioriteit { get; set; }
        [XmlAttribute(AttributeName = "prijs")]
        public string Prijs { get; set; }
    }

    [XmlRoot(ElementName = "optiepakketten")]
    public class Optiepakketten
    {
        [XmlElement(ElementName = "pakket")]
        public List<Pakket> Pakket { get; set; }
    }

    [XmlRoot(ElementName = "accessoires")]
    public class Accessoires
    {
        [XmlElement(ElementName = "accessoire")]
        public List<Accessoire> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "accessoiregroep")]
    public class Accessoiregroep
    {
        [XmlElement(ElementName = "accessoire")]
        public List<Accessoire> Accessoire { get; set; }
        [XmlAttribute(AttributeName = "naam")]
        public string Naam { get; set; }
    }

    [XmlRoot(ElementName = "accessoiregroepen")]
    public class Accessoiregroepen
    {
        [XmlElement(ElementName = "accessoiregroep")]
        public List<Accessoiregroep> Accessoiregroep { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires")]
    public class Zoekaccessoires
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_engels")]
    public class Zoekaccessoires_engels
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_duits")]
    public class Zoekaccessoires_duits
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_frans")]
    public class Zoekaccessoires_frans
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_spaans")]
    public class Zoekaccessoires_spaans
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_portugees")]
    public class Zoekaccessoires_portugees
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_italiaans")]
    public class Zoekaccessoires_italiaans
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_grieks")]
    public class Zoekaccessoires_grieks
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_russisch")]
    public class Zoekaccessoires_russisch
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_roemeens")]
    public class Zoekaccessoires_roemeens
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_hongaars")]
    public class Zoekaccessoires_hongaars
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_pools")]
    public class Zoekaccessoires_pools
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_tsjechisch")]
    public class Zoekaccessoires_tsjechisch
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_bulgaars")]
    public class Zoekaccessoires_bulgaars
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_deens")]
    public class Zoekaccessoires_deens
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_slowaaks")]
    public class Zoekaccessoires_slowaaks
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_turks")]
    public class Zoekaccessoires_turks
    {
        [XmlElement(ElementName = "accessoire")]
        public List<string> Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "schade")]
    public class Schade
    {
        [XmlElement(ElementName = "status")]
        public string Status { get; set; }
        [XmlElement(ElementName = "opmerkingen")]
        public string Opmerkingen { get; set; }
        [XmlElement(ElementName = "beschadigingen")]
        public string Beschadigingen { get; set; }
        [XmlElement(ElementName = "defecte_onderdelen")]
        public string Defecte_onderdelen { get; set; }
    }

    [XmlRoot(ElementName = "vgs")]
    public class Vgs
    {
        [XmlElement(ElementName = "optiesleutel")]
        public string Optiesleutel { get; set; }
        [XmlElement(ElementName = "merktypecode")]
        public string Merktypecode { get; set; }
        [XmlElement(ElementName = "voertuigsoort")]
        public string Voertuigsoort { get; set; }
        [XmlElement(ElementName = "merk")]
        public string Merk { get; set; }
        [XmlElement(ElementName = "model")]
        public string Model { get; set; }
        [XmlElement(ElementName = "type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "uitvoering")]
        public string Uitvoering { get; set; }
        [XmlElement(ElementName = "carrosserievorm")]
        public string Carrosserievorm { get; set; }
        [XmlElement(ElementName = "brandstof")]
        public string Brandstof { get; set; }
    }

    [XmlRoot(ElementName = "autotelex")]
    public class Autotelex
    {
        [XmlElement(ElementName = "uitvoeringsnummer")]
        public string Uitvoeringsnummer { get; set; }
        [XmlElement(ElementName = "voertuigsoort")]
        public string Voertuigsoort { get; set; }
        [XmlElement(ElementName = "merk")]
        public string Merk { get; set; }
        [XmlElement(ElementName = "model")]
        public string Model { get; set; }
        [XmlElement(ElementName = "carvariant")]
        public string Carvariant { get; set; }
        [XmlElement(ElementName = "uitvoering")]
        public string Uitvoering { get; set; }
        [XmlElement(ElementName = "koetswerk")]
        public string Koetswerk { get; set; }
        [XmlElement(ElementName = "brandstof")]
        public string Brandstof { get; set; }
    }

    [XmlRoot(ElementName = "afbeelding")]
    public class Afbeelding
    {
        [XmlElement(ElementName = "bestandsnaam")]
        public string Bestandsnaam { get; set; }
        [XmlElement(ElementName = "omschrijving")]
        public string Omschrijving { get; set; }
        [XmlAttribute(AttributeName = "nr")]
        public string Nr { get; set; }
        public int Nr_nr { get; set; }
    }

    [XmlRoot(ElementName = "afbeeldingen")]
    public class Afbeeldingen
    {
        [XmlElement(ElementName = "afbeelding")]
        public List<Afbeelding> Afbeelding { get; set; }
    }

    [XmlRoot(ElementName = "voertuig")]
    public class Voertuig
    {
        [XmlElement(ElementName = "voertuignr_hexon")]
        public string Voertuignr_hexon { get; set; }
        [XmlElement(ElementName = "voertuignr")]
        public string Voertuignr { get; set; }
        [XmlElement(ElementName = "voertuignr_klant")]
        public string Voertuignr_klant { get; set; }
        [XmlElement(ElementName = "klantnummer")]
        public string Klantnummer { get; set; }
        [XmlElement(ElementName = "invoerdatum")]
        public string Invoerdatum { get; set; }
        public DateTime Invoerdatum_dt { get; set; }
        [XmlElement(ElementName = "kenteken")]
        public string Kenteken { get; set; }
        [XmlElement(ElementName = "merk")]
        public string Merk { get; set; }
        [XmlElement(ElementName = "model")]
        public string Model { get; set; }
        [XmlElement(ElementName = "type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "toevoeging")]
        public string Toevoeging { get; set; }
        [XmlElement(ElementName = "merk_orig")]
        public string Merk_orig { get; set; }
        [XmlElement(ElementName = "model_orig")]
        public string Model_orig { get; set; }
        [XmlElement(ElementName = "type_orig")]
        public string Type_orig { get; set; }
        [XmlElement(ElementName = "voertuigsoort")]
        public string Voertuigsoort { get; set; }
        [XmlElement(ElementName = "carrosserie")]
        public string Carrosserie { get; set; }
        [XmlElement(ElementName = "aantal_deuren")]
        public string Aantal_deuren { get; set; }
        [XmlElement(ElementName = "tellerstand")]
        public Tellerstand Tellerstand { get; set; }
        [XmlElement(ElementName = "brandstof")]
        public string Brandstof { get; set; }
        [XmlElement(ElementName = "transmissie")]
        public string Transmissie { get; set; }
        [XmlElement(ElementName = "aantal_versnellingen")]
        public string Aantal_versnellingen { get; set; }
        [XmlElement(ElementName = "btw_marge")]
        public string Btw_marge { get; set; }
        [XmlElement(ElementName = "nieuw_voertuig")]
        public string Nieuw_voertuig { get; set; }
        [XmlElement(ElementName = "laktint")]
        public string Laktint { get; set; }
        [XmlElement(ElementName = "kleur")]
        public string Kleur { get; set; }
        [XmlElement(ElementName = "basiskleur")]
        public string Basiskleur { get; set; }
        [XmlElement(ElementName = "laksoort")]
        public string Laksoort { get; set; }
        [XmlElement(ElementName = "constructiedatum")]
        public string Constructiedatum { get; set; }
        [XmlElement(ElementName = "bouwjaar")]
        public string Bouwjaar { get; set; }
        [XmlElement(ElementName = "datum_deel_1")]
        public string Datum_deel_1 { get; set; }
        [XmlElement(ElementName = "datum_deel_1a")]
        public string Datum_deel_1a { get; set; }
        [XmlElement(ElementName = "datum_deel_1b")]
        public string Datum_deel_1b { get; set; }
        [XmlElement(ElementName = "verkoopprijs_particulier")]
        public Verkoopprijs_particulier Verkoopprijs_particulier { get; set; }
        [XmlElement(ElementName = "actieprijs")]
        public string Actieprijs { get; set; }
        [XmlElement(ElementName = "verkoopprijs_handel")]
        public string Verkoopprijs_handel { get; set; }
        [XmlElement(ElementName = "veiling")]
        public string Veiling { get; set; }
        [XmlElement(ElementName = "type_hybride")]
        public string Type_hybride { get; set; }
        [XmlElement(ElementName = "hybride_aandrijving")]
        public string Hybride_aandrijving { get; set; }
        [XmlElement(ElementName = "plugin_hybride")]
        public string Plugin_hybride { get; set; }
        [XmlElement(ElementName = "aanwezige_accu")]
        public string Aanwezige_accu { get; set; }
        [XmlElement(ElementName = "accutype")]
        public string Accutype { get; set; }
        [XmlElement(ElementName = "accu_laadvermogen")]
        public string Accu_laadvermogen { get; set; }
        [XmlElement(ElementName = "accu_laadtijd")]
        public string Accu_laadtijd { get; set; }
        [XmlElement(ElementName = "accu_snellaadtijd")]
        public string Accu_snellaadtijd { get; set; }
        [XmlElement(ElementName = "accu_laadsnelheid")]
        public string Accu_laadsnelheid { get; set; }
        [XmlElement(ElementName = "accu_snellaadsnelheid")]
        public string Accu_snellaadsnelheid { get; set; }
        [XmlElement(ElementName = "accu_conditie")]
        public string Accu_conditie { get; set; }
        [XmlElement(ElementName = "aantal_fasen_acculader")]
        public string Aantal_fasen_acculader { get; set; }
        [XmlElement(ElementName = "stekkeraansluiting")]
        public string Stekkeraansluiting { get; set; }
        [XmlElement(ElementName = "stekkeraansluiting_snellader")]
        public string Stekkeraansluiting_snellader { get; set; }
        [XmlElement(ElementName = "geschikt_voor_snelladen")]
        public string Geschikt_voor_snelladen { get; set; }
        [XmlElement(ElementName = "apk")]
        public Apk Apk { get; set; }
        [XmlElement(ElementName = "massa")]
        public string Massa { get; set; }
        [XmlElement(ElementName = "max_trekgewicht")]
        public string Max_trekgewicht { get; set; }
        [XmlElement(ElementName = "cilinder_aantal")]
        public string Cilinder_aantal { get; set; }
        [XmlElement(ElementName = "cilinder_inhoud")]
        public string Cilinder_inhoud { get; set; }
        [XmlElement(ElementName = "vermogen_motor_kw")]
        public string Vermogen_motor_kw { get; set; }
        [XmlElement(ElementName = "vermogen_motor_pk")]
        public string Vermogen_motor_pk { get; set; }
        [XmlElement(ElementName = "vermogen_brandstof_kw")]
        public string Vermogen_brandstof_kw { get; set; }
        [XmlElement(ElementName = "vermogen_brandstof_pk")]
        public string Vermogen_brandstof_pk { get; set; }
        [XmlElement(ElementName = "vermogen_elektrisch_kw")]
        public string Vermogen_elektrisch_kw { get; set; }
        [XmlElement(ElementName = "vermogen_elektrisch_pk")]
        public string Vermogen_elektrisch_pk { get; set; }
        [XmlElement(ElementName = "koppel")]
        public string Koppel { get; set; }
        [XmlElement(ElementName = "aantal_zitplaatsen")]
        public string Aantal_zitplaatsen { get; set; }
        [XmlElement(ElementName = "bpm_bedrag")]
        public string Bpm_bedrag { get; set; }
        [XmlElement(ElementName = "interieurkleur")]
        public string Interieurkleur { get; set; }
        [XmlElement(ElementName = "bekleding")]
        public string Bekleding { get; set; }
        [XmlElement(ElementName = "aantal_sleutels")]
        public string Aantal_sleutels { get; set; }
        [XmlElement(ElementName = "aantal_handzenders")]
        public string Aantal_handzenders { get; set; }
        [XmlElement(ElementName = "code_pas_sleutel")]
        public string Code_pas_sleutel { get; set; }
        [XmlElement(ElementName = "nap_weblabel")]
        public string Nap_weblabel { get; set; }
        [XmlElement(ElementName = "carpass")]
        public string Carpass { get; set; }
        [XmlElement(ElementName = "carpass_url")]
        public string Carpass_url { get; set; }
        [XmlElement(ElementName = "onderhoudsboekjes")]
        public string Onderhoudsboekjes { get; set; }
        [XmlElement(ElementName = "locatie_voertuig")]
        public string Locatie_voertuig { get; set; }
        [XmlElement(ElementName = "exportprijs")]
        public string Exportprijs { get; set; }
        [XmlElement(ElementName = "meeneemprijs")]
        public string Meeneemprijs { get; set; }
        [XmlElement(ElementName = "inkoopprijs")]
        public string Inkoopprijs { get; set; }
        [XmlElement(ElementName = "bieden_vanaf")]
        public Bieden_vanaf Bieden_vanaf { get; set; }
        [XmlElement(ElementName = "reserveringsprijs")]
        public string Reserveringsprijs { get; set; }
        [XmlElement(ElementName = "opknapkosten")]
        public string Opknapkosten { get; set; }
        [XmlElement(ElementName = "kosten_rijklaar")]
        public string Kosten_rijklaar { get; set; }
        [XmlElement(ElementName = "gemiddeld_verbruik")]
        public string Gemiddeld_verbruik { get; set; }
        [XmlElement(ElementName = "schadevoertuig")]
        public string Schadevoertuig { get; set; }
        [XmlElement(ElementName = "consignatie")]
        public string Consignatie { get; set; }
        [XmlElement(ElementName = "demovoertuig")]
        public string Demovoertuig { get; set; }
        [XmlElement(ElementName = "wielbasis")]
        public string Wielbasis { get; set; }
        [XmlElement(ElementName = "laadvermogen")]
        public string Laadvermogen { get; set; }
        [XmlElement(ElementName = "gvw")]
        public string Gvw { get; set; }
        [XmlElement(ElementName = "loopvlakbreedte")]
        public string Loopvlakbreedte { get; set; }
        [XmlElement(ElementName = "bandhoogte")]
        public string Bandhoogte { get; set; }
        [XmlElement(ElementName = "aantal_assen")]
        public string Aantal_assen { get; set; }
        [XmlElement(ElementName = "lengte")]
        public string Lengte { get; set; }
        [XmlElement(ElementName = "breedte")]
        public string Breedte { get; set; }
        [XmlElement(ElementName = "hoogte")]
        public string Hoogte { get; set; }
        [XmlElement(ElementName = "vin")]
        public string Vin { get; set; }
        [XmlElement(ElementName = "emissieklasse")]
        public string Emissieklasse { get; set; }
        [XmlElement(ElementName = "energielabel")]
        public string Energielabel { get; set; }
        [XmlElement(ElementName = "volautomatisch")]
        public string Volautomatisch { get; set; }
        [XmlElement(ElementName = "voortbeweging_zelfstandig")]
        public string Voortbeweging_zelfstandig { get; set; }
        [XmlElement(ElementName = "verplaatsing")]
        public string Verplaatsing { get; set; }
        [XmlElement(ElementName = "bediening")]
        public string Bediening { get; set; }
        [XmlElement(ElementName = "generatorvermogen")]
        public string Generatorvermogen { get; set; }
        [XmlElement(ElementName = "compressorsoort")]
        public string Compressorsoort { get; set; }
        [XmlElement(ElementName = "doorvoercapaciteit")]
        public string Doorvoercapaciteit { get; set; }
        [XmlElement(ElementName = "maximale_reikwijdte")]
        public string Maximale_reikwijdte { get; set; }
        [XmlElement(ElementName = "voedingsspanning")]
        public string Voedingsspanning { get; set; }
        [XmlElement(ElementName = "mastlengte")]
        public string Mastlengte { get; set; }
        [XmlElement(ElementName = "bovenarmlengte")]
        public string Bovenarmlengte { get; set; }
        [XmlElement(ElementName = "rupsbandbreedte")]
        public string Rupsbandbreedte { get; set; }
        [XmlElement(ElementName = "laadschopinhoud")]
        public string Laadschopinhoud { get; set; }
        [XmlElement(ElementName = "minimale_koeltemperatuur")]
        public string Minimale_koeltemperatuur { get; set; }
        [XmlElement(ElementName = "maximale_koeltemperatuur")]
        public string Maximale_koeltemperatuur { get; set; }
        [XmlElement(ElementName = "toepassingsmateriaal")]
        public string Toepassingsmateriaal { get; set; }
        [XmlElement(ElementName = "toepassingsgebied")]
        public string Toepassingsgebied { get; set; }
        [XmlElement(ElementName = "toepassingsschalen")]
        public string Toepassingsschalen { get; set; }
        [XmlElement(ElementName = "ce_markering")]
        public string Ce_markering { get; set; }
        [XmlElement(ElementName = "staat_algemeen")]
        public string Staat_algemeen { get; set; }
        [XmlElement(ElementName = "staat_technisch")]
        public string Staat_technisch { get; set; }
        [XmlElement(ElementName = "staat_optisch")]
        public string Staat_optisch { get; set; }
        [XmlElement(ElementName = "diepte")]
        public string Diepte { get; set; }
        [XmlElement(ElementName = "rompmateriaal")]
        public string Rompmateriaal { get; set; }
        [XmlElement(ElementName = "ligplaats")]
        public string Ligplaats { get; set; }
        [XmlElement(ElementName = "aantal_staanplaatsen")]
        public string Aantal_staanplaatsen { get; set; }
        [XmlElement(ElementName = "merk_motor")]
        public string Merk_motor { get; set; }
        [XmlElement(ElementName = "type_motor")]
        public string Type_motor { get; set; }
        [XmlElement(ElementName = "land_nieuw")]
        public string Land_nieuw { get; set; }
        [XmlElement(ElementName = "aandrijving")]
        public string Aandrijving { get; set; }
        [XmlElement(ElementName = "prijs_nieuw_herberekend")]
        public string Prijs_nieuw_herberekend { get; set; }
        [XmlElement(ElementName = "werkbreedte")]
        public string Werkbreedte { get; set; }
        [XmlElement(ElementName = "werkhoogte")]
        public string Werkhoogte { get; set; }
        [XmlElement(ElementName = "hefhoogte")]
        public string Hefhoogte { get; set; }
        [XmlElement(ElementName = "vrije_hefhoogte")]
        public string Vrije_hefhoogte { get; set; }
        [XmlElement(ElementName = "hefcapaciteit")]
        public string Hefcapaciteit { get; set; }
        [XmlElement(ElementName = "aantal_wielen")]
        public string Aantal_wielen { get; set; }
        [XmlElement(ElementName = "mast")]
        public string Mast { get; set; }
        [XmlElement(ElementName = "mast_aantal_delen")]
        public string Mast_aantal_delen { get; set; }
        [XmlElement(ElementName = "voortbeweging")]
        public string Voortbeweging { get; set; }
        [XmlElement(ElementName = "chassisvorm")]
        public string Chassisvorm { get; set; }
        [XmlElement(ElementName = "besturing")]
        public string Besturing { get; set; }
        [XmlElement(ElementName = "fiets_uitvoering")]
        public string Fiets_uitvoering { get; set; }
        [XmlElement(ElementName = "typenummer")]
        public string Typenummer { get; set; }
        [XmlElement(ElementName = "modeldatum_vanaf")]
        public string Modeldatum_vanaf { get; set; }
        [XmlElement(ElementName = "modeldatum_tot")]
        public string Modeldatum_tot { get; set; }
        [XmlElement(ElementName = "modelcode")]
        public string Modelcode { get; set; }
        [XmlElement(ElementName = "vorklengte")]
        public string Vorklengte { get; set; }
        [XmlElement(ElementName = "vorkbreedte")]
        public string Vorkbreedte { get; set; }
        [XmlElement(ElementName = "lastzwaartepunt")]
        public string Lastzwaartepunt { get; set; }
        [XmlElement(ElementName = "accucapaciteit")]
        public string Accucapaciteit { get; set; }
        [XmlElement(ElementName = "accuspanning")]
        public string Accuspanning { get; set; }
        [XmlElement(ElementName = "accubouwjaar")]
        public string Accubouwjaar { get; set; }
        [XmlElement(ElementName = "accubouwmaand")]
        public string Accubouwmaand { get; set; }
        [XmlElement(ElementName = "accumerk")]
        public string Accumerk { get; set; }
        [XmlElement(ElementName = "pomp_aansluitingsmaat")]
        public string Pomp_aansluitingsmaat { get; set; }
        [XmlElement(ElementName = "attachment_passend_op")]
        public string Attachment_passend_op { get; set; }
        [XmlElement(ElementName = "hefwerktuigaansluiting")]
        public string Hefwerktuigaansluiting { get; set; }
        [XmlElement(ElementName = "snelwisselsysteem")]
        public string Snelwisselsysteem { get; set; }
        [XmlElement(ElementName = "pantograaf")]
        public string Pantograaf { get; set; }
        [XmlElement(ElementName = "breker_soort")]
        public string Breker_soort { get; set; }
        [XmlElement(ElementName = "wals_trillend")]
        public string Wals_trillend { get; set; }
        [XmlElement(ElementName = "wals_oppervlak")]
        public string Wals_oppervlak { get; set; }
        [XmlElement(ElementName = "serienummer")]
        public string Serienummer { get; set; }
        [XmlElement(ElementName = "prijstype")]
        public string Prijstype { get; set; }
        [XmlElement(ElementName = "bieden_toestaan")]
        public string Bieden_toestaan { get; set; }
        [XmlElement(ElementName = "oldtimer")]
        public string Oldtimer { get; set; }
        [XmlElement(ElementName = "onderdeel_geschikt_voor")]
        public string Onderdeel_geschikt_voor { get; set; }
        [XmlElement(ElementName = "ktype")]
        public string Ktype { get; set; }
        [XmlElement(ElementName = "fiets_versnelling")]
        public string Fiets_versnelling { get; set; }
        [XmlElement(ElementName = "vrachtwagen_transmissie")]
        public string Vrachtwagen_transmissie { get; set; }
        [XmlElement(ElementName = "powershuttle")]
        public string Powershuttle { get; set; }
        [XmlElement(ElementName = "gesynchroniseerde_transmissie")]
        public string Gesynchroniseerde_transmissie { get; set; }
        [XmlElement(ElementName = "transmissie_groepen")]
        public string Transmissie_groepen { get; set; }
        [XmlElement(ElementName = "maaier_soort")]
        public string Maaier_soort { get; set; }
        [XmlElement(ElementName = "seizoen")]
        public string Seizoen { get; set; }
        [XmlElement(ElementName = "bedoelde_positie_voor_achter")]
        public string Bedoelde_positie_voor_achter { get; set; }
        [XmlElement(ElementName = "bedoelde_positie_links_rechts")]
        public string Bedoelde_positie_links_rechts { get; set; }
        [XmlElement(ElementName = "minimaal_octaangetal")]
        public string Minimaal_octaangetal { get; set; }
        [XmlElement(ElementName = "e10_geschikt")]
        public string E10_geschikt { get; set; }
        [XmlElement(ElementName = "framehoogte")]
        public string Framehoogte { get; set; }
        [XmlElement(ElementName = "maximale_doorrijhoogte")]
        public string Maximale_doorrijhoogte { get; set; }
        [XmlElement(ElementName = "leasevoorstellen")]
        public string Leasevoorstellen { get; set; }
        [XmlElement(ElementName = "asconfiguratie")]
        public Asconfiguratie Asconfiguratie { get; set; }
        [XmlElement(ElementName = "nieuwprijs")]
        public string Nieuwprijs { get; set; }
        [XmlElement(ElementName = "netto_catalogusprijs")]
        public string Netto_catalogusprijs { get; set; }
        [XmlElement(ElementName = "fiscale_waarde")]
        public string Fiscale_waarde { get; set; }
        [XmlElement(ElementName = "bijtelling_pct")]
        public string Bijtelling_pct { get; set; }
        [XmlElement(ElementName = "verkocht_datum")]
        public string Verkocht_datum { get; set; }
        [XmlElement(ElementName = "eigen_garantielabel")]
        public string Eigen_garantielabel { get; set; }
        [XmlElement(ElementName = "verlengd")]
        public string Verlengd { get; set; }
        [XmlElement(ElementName = "verhoogd")]
        public string Verhoogd { get; set; }
        [XmlElement(ElementName = "assen_aangedreven")]
        public string Assen_aangedreven { get; set; }
        [XmlElement(ElementName = "bedrijfsuren")]
        public string Bedrijfsuren { get; set; }
        [XmlElement(ElementName = "geremd")]
        public string Geremd { get; set; }
        [XmlElement(ElementName = "datum_binnenkomst")]
        public string Datum_binnenkomst { get; set; }
        [XmlElement(ElementName = "verhuur")]
        public string Verhuur { get; set; }
        [XmlElement(ElementName = "klassieker")]
        public string Klassieker { get; set; }
        [XmlElement(ElementName = "aantal_eigenaren")]
        public string Aantal_eigenaren { get; set; }
        [XmlElement(ElementName = "onderhoudshistorie")]
        public string Onderhoudshistorie { get; set; }
        [XmlElement(ElementName = "afleverpakketten")]
        public Afleverpakketten Afleverpakketten { get; set; }
        [XmlElement(ElementName = "afleverpakket_toegepast")]
        public string Afleverpakket_toegepast { get; set; }
        [XmlElement(ElementName = "aantal_slaapplaatsen")]
        public string Aantal_slaapplaatsen { get; set; }
        [XmlElement(ElementName = "merk_chassis")]
        public string Merk_chassis { get; set; }
        [XmlElement(ElementName = "model_chassis")]
        public string Model_chassis { get; set; }
        [XmlElement(ElementName = "meldcode")]
        public string Meldcode { get; set; }
        [XmlElement(ElementName = "land")]
        public string Land { get; set; }
        [XmlElement(ElementName = "topsnelheid")]
        public string Topsnelheid { get; set; }
        [XmlElement(ElementName = "tankinhoud")]
        public string Tankinhoud { get; set; }
        [XmlElement(ElementName = "actieradius")]
        public string Actieradius { get; set; }
        [XmlElement(ElementName = "actieradius_elektrisch")]
        public string Actieradius_elektrisch { get; set; }
        [XmlElement(ElementName = "actieradius_brandstof")]
        public string Actieradius_brandstof { get; set; }
        [XmlElement(ElementName = "max_trekgewicht_ongeremd")]
        public string Max_trekgewicht_ongeremd { get; set; }
        [XmlElement(ElementName = "acceleratie")]
        public string Acceleratie { get; set; }
        [XmlElement(ElementName = "max_dakbelasting")]
        public string Max_dakbelasting { get; set; }
        [XmlElement(ElementName = "inhoud_laadruimte_banken_weggeklapt")]
        public string Inhoud_laadruimte_banken_weggeklapt { get; set; }
        [XmlElement(ElementName = "verbruik_stad")]
        public string Verbruik_stad { get; set; }
        [XmlElement(ElementName = "verbruik_snelweg")]
        public string Verbruik_snelweg { get; set; }
        [XmlElement(ElementName = "elektriciteitsverbruik_gemiddeld")]
        public string Elektriciteitsverbruik_gemiddeld { get; set; }
        [XmlElement(ElementName = "elektriciteitsverbruik_snelweg")]
        public string Elektriciteitsverbruik_snelweg { get; set; }
        [XmlElement(ElementName = "elektriciteitsverbruik_stad")]
        public string Elektriciteitsverbruik_stad { get; set; }
        [XmlElement(ElementName = "co2_uitstoot")]
        public string Co2_uitstoot { get; set; }
        [XmlElement(ElementName = "fijnstof_uitstoot")]
        public string Fijnstof_uitstoot { get; set; }
        [XmlElement(ElementName = "munteenheid")]
        public string Munteenheid { get; set; }
        [XmlElement(ElementName = "cabine_soort")]
        public string Cabine_soort { get; set; }
        [XmlElement(ElementName = "cabine_type")]
        public string Cabine_type { get; set; }
        [XmlElement(ElementName = "cabine_lengte")]
        public string Cabine_lengte { get; set; }
        [XmlElement(ElementName = "transmissie_merk")]
        public string Transmissie_merk { get; set; }
        [XmlElement(ElementName = "transmissie_type")]
        public string Transmissie_type { get; set; }
        [XmlElement(ElementName = "alternatieve_onderdelen")]
        public string Alternatieve_onderdelen { get; set; }
        [XmlElement(ElementName = "fabrieksgarantie_tot")]
        public string Fabrieksgarantie_tot { get; set; }
        [XmlElement(ElementName = "merkgarantie")]
        public string Merkgarantie { get; set; }
        [XmlElement(ElementName = "autotrust_garantie")]
        public string Autotrust_garantie { get; set; }
        [XmlElement(ElementName = "vakgarant_premium_occasion")]
        public string Vakgarant_premium_occasion { get; set; }
        [XmlElement(ElementName = "vwe_occasion_garant_plan")]
        public string Vwe_occasion_garant_plan { get; set; }
        [XmlElement(ElementName = "cargarantie")]
        public string Cargarantie { get; set; }
        [XmlElement(ElementName = "honderd_procent_onderhouden")]
        public string Honderd_procent_onderhouden { get; set; }
        [XmlElement(ElementName = "bovag_garantie")]
        public string Bovag_garantie { get; set; }
        [XmlElement(ElementName = "bovag_40puntencheck")]
        public string Bovag_40puntencheck { get; set; }
        [XmlElement(ElementName = "bovag_afleverbeurt")]
        public string Bovag_afleverbeurt { get; set; }
        [XmlElement(ElementName = "garantie_maanden")]
        public string Garantie_maanden { get; set; }
        [XmlElement(ElementName = "garantie_km")]
        public string Garantie_km { get; set; }
        [XmlElement(ElementName = "opmerkingen_garantie")]
        public string Opmerkingen_garantie { get; set; }
        [XmlElement(ElementName = "euro_ncap_rating")]
        public string Euro_ncap_rating { get; set; }
        [XmlElement(ElementName = "bed1_type")]
        public string Bed1_type { get; set; }
        [XmlElement(ElementName = "bed1_positie")]
        public string Bed1_positie { get; set; }
        [XmlElement(ElementName = "bed1_richting")]
        public string Bed1_richting { get; set; }
        [XmlElement(ElementName = "bed2_type")]
        public string Bed2_type { get; set; }
        [XmlElement(ElementName = "bed2_positie")]
        public string Bed2_positie { get; set; }
        [XmlElement(ElementName = "bed2_richting")]
        public string Bed2_richting { get; set; }
        [XmlElement(ElementName = "bed3_type")]
        public string Bed3_type { get; set; }
        [XmlElement(ElementName = "bed3_positie")]
        public string Bed3_positie { get; set; }
        [XmlElement(ElementName = "bed3_richting")]
        public string Bed3_richting { get; set; }
        [XmlElement(ElementName = "zit1_type")]
        public string Zit1_type { get; set; }
        [XmlElement(ElementName = "zit1_positie")]
        public string Zit1_positie { get; set; }
        [XmlElement(ElementName = "zit2_type")]
        public string Zit2_type { get; set; }
        [XmlElement(ElementName = "zit2_positie")]
        public string Zit2_positie { get; set; }
        [XmlElement(ElementName = "keuken_positie")]
        public string Keuken_positie { get; set; }
        [XmlElement(ElementName = "sanitair_opstelling")]
        public string Sanitair_opstelling { get; set; }
        [XmlElement(ElementName = "directe_link")]
        public string Directe_link { get; set; }
        [XmlElement(ElementName = "contactpersonen")]
        public string Contactpersonen { get; set; }
        [XmlElement(ElementName = "opmerkingen")]
        public string Opmerkingen { get; set; }
        [XmlElement(ElementName = "opmerkingen_engels")]
        public string Opmerkingen_engels { get; set; }
        [XmlElement(ElementName = "opmerkingen_duits")]
        public string Opmerkingen_duits { get; set; }
        [XmlElement(ElementName = "opmerkingen_frans")]
        public string Opmerkingen_frans { get; set; }
        [XmlElement(ElementName = "opmerkingen_spaans")]
        public string Opmerkingen_spaans { get; set; }
        [XmlElement(ElementName = "opmerkingen_portugees")]
        public string Opmerkingen_portugees { get; set; }
        [XmlElement(ElementName = "opmerkingen_italiaans")]
        public string Opmerkingen_italiaans { get; set; }
        [XmlElement(ElementName = "opmerkingen_grieks")]
        public string Opmerkingen_grieks { get; set; }
        [XmlElement(ElementName = "opmerkingen_russisch")]
        public string Opmerkingen_russisch { get; set; }
        [XmlElement(ElementName = "opmerkingen_roemeens")]
        public string Opmerkingen_roemeens { get; set; }
        [XmlElement(ElementName = "opmerkingen_hongaars")]
        public string Opmerkingen_hongaars { get; set; }
        [XmlElement(ElementName = "opmerkingen_pools")]
        public string Opmerkingen_pools { get; set; }
        [XmlElement(ElementName = "opmerkingen_tsjechisch")]
        public string Opmerkingen_tsjechisch { get; set; }
        [XmlElement(ElementName = "opmerkingen_bulgaars")]
        public string Opmerkingen_bulgaars { get; set; }
        [XmlElement(ElementName = "opmerkingen_kroatisch")]
        public string Opmerkingen_kroatisch { get; set; }
        [XmlElement(ElementName = "opmerkingen_deens")]
        public string Opmerkingen_deens { get; set; }
        [XmlElement(ElementName = "opmerkingen_slowaaks")]
        public string Opmerkingen_slowaaks { get; set; }
        [XmlElement(ElementName = "opmerkingen_turks")]
        public string Opmerkingen_turks { get; set; }
        [XmlElement(ElementName = "opmerkingen_fins")]
        public string Opmerkingen_fins { get; set; }
        [XmlElement(ElementName = "opmerkingen_zweeds")]
        public string Opmerkingen_zweeds { get; set; }
        [XmlElement(ElementName = "opmerkingen_noors")]
        public string Opmerkingen_noors { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel")]
        public string Opmerkingen_handel { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_engels")]
        public string Opmerkingen_handel_engels { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_duits")]
        public string Opmerkingen_handel_duits { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_frans")]
        public string Opmerkingen_handel_frans { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_spaans")]
        public string Opmerkingen_handel_spaans { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_portugees")]
        public string Opmerkingen_handel_portugees { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_italiaans")]
        public string Opmerkingen_handel_italiaans { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_grieks")]
        public string Opmerkingen_handel_grieks { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_russisch")]
        public string Opmerkingen_handel_russisch { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_roemeens")]
        public string Opmerkingen_handel_roemeens { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_hongaars")]
        public string Opmerkingen_handel_hongaars { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_pools")]
        public string Opmerkingen_handel_pools { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_tsjechisch")]
        public string Opmerkingen_handel_tsjechisch { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_bulgaars")]
        public string Opmerkingen_handel_bulgaars { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_kroatisch")]
        public string Opmerkingen_handel_kroatisch { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_deens")]
        public string Opmerkingen_handel_deens { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_slowaaks")]
        public string Opmerkingen_handel_slowaaks { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_turks")]
        public string Opmerkingen_handel_turks { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_fins")]
        public string Opmerkingen_handel_fins { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_zweeds")]
        public string Opmerkingen_handel_zweeds { get; set; }
        [XmlElement(ElementName = "opmerkingen_handel_noors")]
        public string Opmerkingen_handel_noors { get; set; }
        [XmlElement(ElementName = "titel")]
        public string Titel { get; set; }
        public string Titel_url { get; set; }
        [XmlElement(ElementName = "titel_engels")]
        public string Titel_engels { get; set; }
        [XmlElement(ElementName = "titel_duits")]
        public string Titel_duits { get; set; }
        [XmlElement(ElementName = "titel_frans")]
        public string Titel_frans { get; set; }
        [XmlElement(ElementName = "titel_spaans")]
        public string Titel_spaans { get; set; }
        [XmlElement(ElementName = "titel_portugees")]
        public string Titel_portugees { get; set; }
        [XmlElement(ElementName = "titel_italiaans")]
        public string Titel_italiaans { get; set; }
        [XmlElement(ElementName = "titel_grieks")]
        public string Titel_grieks { get; set; }
        [XmlElement(ElementName = "titel_russisch")]
        public string Titel_russisch { get; set; }
        [XmlElement(ElementName = "titel_roemeens")]
        public string Titel_roemeens { get; set; }
        [XmlElement(ElementName = "titel_hongaars")]
        public string Titel_hongaars { get; set; }
        [XmlElement(ElementName = "titel_pools")]
        public string Titel_pools { get; set; }
        [XmlElement(ElementName = "titel_tsjechisch")]
        public string Titel_tsjechisch { get; set; }
        [XmlElement(ElementName = "titel_bulgaars")]
        public string Titel_bulgaars { get; set; }
        [XmlElement(ElementName = "titel_kroatisch")]
        public string Titel_kroatisch { get; set; }
        [XmlElement(ElementName = "titel_deens")]
        public string Titel_deens { get; set; }
        [XmlElement(ElementName = "titel_slowaaks")]
        public string Titel_slowaaks { get; set; }
        [XmlElement(ElementName = "titel_turks")]
        public string Titel_turks { get; set; }
        [XmlElement(ElementName = "titel_fins")]
        public string Titel_fins { get; set; }
        [XmlElement(ElementName = "titel_zweeds")]
        public string Titel_zweeds { get; set; }
        [XmlElement(ElementName = "titel_noors")]
        public string Titel_noors { get; set; }
        [XmlElement(ElementName = "highlights")]
        public string Highlights { get; set; }
        [XmlElement(ElementName = "highlights_engels")]
        public string Highlights_engels { get; set; }
        [XmlElement(ElementName = "highlights_duits")]
        public string Highlights_duits { get; set; }
        [XmlElement(ElementName = "highlights_frans")]
        public string Highlights_frans { get; set; }
        [XmlElement(ElementName = "highlights_spaans")]
        public string Highlights_spaans { get; set; }
        [XmlElement(ElementName = "highlights_portugees")]
        public string Highlights_portugees { get; set; }
        [XmlElement(ElementName = "highlights_italiaans")]
        public string Highlights_italiaans { get; set; }
        [XmlElement(ElementName = "highlights_grieks")]
        public string Highlights_grieks { get; set; }
        [XmlElement(ElementName = "highlights_russisch")]
        public string Highlights_russisch { get; set; }
        [XmlElement(ElementName = "highlights_roemeens")]
        public string Highlights_roemeens { get; set; }
        [XmlElement(ElementName = "highlights_hongaars")]
        public string Highlights_hongaars { get; set; }
        [XmlElement(ElementName = "highlights_pools")]
        public string Highlights_pools { get; set; }
        [XmlElement(ElementName = "highlights_tsjechisch")]
        public string Highlights_tsjechisch { get; set; }
        [XmlElement(ElementName = "highlights_bulgaars")]
        public string Highlights_bulgaars { get; set; }
        [XmlElement(ElementName = "highlights_kroatisch")]
        public string Highlights_kroatisch { get; set; }
        [XmlElement(ElementName = "highlights_deens")]
        public string Highlights_deens { get; set; }
        [XmlElement(ElementName = "highlights_slowaaks")]
        public string Highlights_slowaaks { get; set; }
        [XmlElement(ElementName = "highlights_turks")]
        public string Highlights_turks { get; set; }
        [XmlElement(ElementName = "highlights_fins")]
        public string Highlights_fins { get; set; }
        [XmlElement(ElementName = "highlights_zweeds")]
        public string Highlights_zweeds { get; set; }
        [XmlElement(ElementName = "highlights_noors")]
        public string Highlights_noors { get; set; }
        [XmlElement(ElementName = "zoektermen")]
        public string Zoektermen { get; set; }
        [XmlElement(ElementName = "zoektermen_engels")]
        public string Zoektermen_engels { get; set; }
        [XmlElement(ElementName = "zoektermen_duits")]
        public string Zoektermen_duits { get; set; }
        [XmlElement(ElementName = "zoektermen_frans")]
        public string Zoektermen_frans { get; set; }
        [XmlElement(ElementName = "zoektermen_spaans")]
        public string Zoektermen_spaans { get; set; }
        [XmlElement(ElementName = "zoektermen_portugees")]
        public string Zoektermen_portugees { get; set; }
        [XmlElement(ElementName = "zoektermen_italiaans")]
        public string Zoektermen_italiaans { get; set; }
        [XmlElement(ElementName = "zoektermen_grieks")]
        public string Zoektermen_grieks { get; set; }
        [XmlElement(ElementName = "zoektermen_russisch")]
        public string Zoektermen_russisch { get; set; }
        [XmlElement(ElementName = "zoektermen_roemeens")]
        public string Zoektermen_roemeens { get; set; }
        [XmlElement(ElementName = "zoektermen_hongaars")]
        public string Zoektermen_hongaars { get; set; }
        [XmlElement(ElementName = "zoektermen_pools")]
        public string Zoektermen_pools { get; set; }
        [XmlElement(ElementName = "zoektermen_tsjechisch")]
        public string Zoektermen_tsjechisch { get; set; }
        [XmlElement(ElementName = "zoektermen_bulgaars")]
        public string Zoektermen_bulgaars { get; set; }
        [XmlElement(ElementName = "zoektermen_kroatisch")]
        public string Zoektermen_kroatisch { get; set; }
        [XmlElement(ElementName = "zoektermen_deens")]
        public string Zoektermen_deens { get; set; }
        [XmlElement(ElementName = "zoektermen_slowaaks")]
        public string Zoektermen_slowaaks { get; set; }
        [XmlElement(ElementName = "zoektermen_turks")]
        public string Zoektermen_turks { get; set; }
        [XmlElement(ElementName = "zoektermen_fins")]
        public string Zoektermen_fins { get; set; }
        [XmlElement(ElementName = "zoektermen_zweeds")]
        public string Zoektermen_zweeds { get; set; }
        [XmlElement(ElementName = "zoektermen_noors")]
        public string Zoektermen_noors { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen")]
        public string Standaardopmerkingen { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_engels")]
        public string Standaardopmerkingen_engels { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_duits")]
        public string Standaardopmerkingen_duits { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_frans")]
        public string Standaardopmerkingen_frans { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_spaans")]
        public string Standaardopmerkingen_spaans { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_portugees")]
        public string Standaardopmerkingen_portugees { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_italiaans")]
        public string Standaardopmerkingen_italiaans { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_grieks")]
        public string Standaardopmerkingen_grieks { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_russisch")]
        public string Standaardopmerkingen_russisch { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_roemeens")]
        public string Standaardopmerkingen_roemeens { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_hongaars")]
        public string Standaardopmerkingen_hongaars { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_pools")]
        public string Standaardopmerkingen_pools { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_tsjechisch")]
        public string Standaardopmerkingen_tsjechisch { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_bulgaars")]
        public string Standaardopmerkingen_bulgaars { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_kroatisch")]
        public string Standaardopmerkingen_kroatisch { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_deens")]
        public string Standaardopmerkingen_deens { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_slowaaks")]
        public string Standaardopmerkingen_slowaaks { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_turks")]
        public string Standaardopmerkingen_turks { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_fins")]
        public string Standaardopmerkingen_fins { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_zweeds")]
        public string Standaardopmerkingen_zweeds { get; set; }
        [XmlElement(ElementName = "standaardopmerkingen_noors")]
        public string Standaardopmerkingen_noors { get; set; }
        [XmlElement(ElementName = "wegenbelasting_kwartaal")]
        public Wegenbelasting_kwartaal Wegenbelasting_kwartaal { get; set; }
        [XmlElement(ElementName = "verkocht")]
        public string Verkocht { get; set; }
        [XmlElement(ElementName = "verwacht")]
        public string Verwacht { get; set; }
        [XmlElement(ElementName = "gereserveerd")]
        public string Gereserveerd { get; set; }
        [XmlElement(ElementName = "optiepakketten")]
        public Optiepakketten Optiepakketten { get; set; }
        [XmlElement(ElementName = "optiepakketten_engels")]
        public string Optiepakketten_engels { get; set; }
        [XmlElement(ElementName = "optiepakketten_duits")]
        public string Optiepakketten_duits { get; set; }
        [XmlElement(ElementName = "optiepakketten_frans")]
        public string Optiepakketten_frans { get; set; }
        [XmlElement(ElementName = "optiepakketten_spaans")]
        public string Optiepakketten_spaans { get; set; }
        [XmlElement(ElementName = "optiepakketten_portugees")]
        public string Optiepakketten_portugees { get; set; }
        [XmlElement(ElementName = "optiepakketten_italiaans")]
        public string Optiepakketten_italiaans { get; set; }
        [XmlElement(ElementName = "optiepakketten_grieks")]
        public string Optiepakketten_grieks { get; set; }
        [XmlElement(ElementName = "optiepakketten_russisch")]
        public string Optiepakketten_russisch { get; set; }
        [XmlElement(ElementName = "optiepakketten_roemeens")]
        public string Optiepakketten_roemeens { get; set; }
        [XmlElement(ElementName = "optiepakketten_hongaars")]
        public string Optiepakketten_hongaars { get; set; }
        [XmlElement(ElementName = "optiepakketten_pools")]
        public string Optiepakketten_pools { get; set; }
        [XmlElement(ElementName = "optiepakketten_tsjechisch")]
        public string Optiepakketten_tsjechisch { get; set; }
        [XmlElement(ElementName = "optiepakketten_bulgaars")]
        public string Optiepakketten_bulgaars { get; set; }
        [XmlElement(ElementName = "optiepakketten_kroatisch")]
        public string Optiepakketten_kroatisch { get; set; }
        [XmlElement(ElementName = "optiepakketten_deens")]
        public string Optiepakketten_deens { get; set; }
        [XmlElement(ElementName = "optiepakketten_slowaaks")]
        public string Optiepakketten_slowaaks { get; set; }
        [XmlElement(ElementName = "optiepakketten_turks")]
        public string Optiepakketten_turks { get; set; }
        [XmlElement(ElementName = "optiepakketten_fins")]
        public string Optiepakketten_fins { get; set; }
        [XmlElement(ElementName = "optiepakketten_zweeds")]
        public string Optiepakketten_zweeds { get; set; }
        [XmlElement(ElementName = "optiepakketten_noors")]
        public string Optiepakketten_noors { get; set; }
        [XmlElement(ElementName = "accessoires")]
        public Accessoires Accessoires { get; set; }
        [XmlElement(ElementName = "accessoires_engels")]
        public string Accessoires_engels { get; set; }
        [XmlElement(ElementName = "accessoires_duits")]
        public string Accessoires_duits { get; set; }
        [XmlElement(ElementName = "accessoires_frans")]
        public string Accessoires_frans { get; set; }
        [XmlElement(ElementName = "accessoires_spaans")]
        public string Accessoires_spaans { get; set; }
        [XmlElement(ElementName = "accessoires_portugees")]
        public string Accessoires_portugees { get; set; }
        [XmlElement(ElementName = "accessoires_italiaans")]
        public string Accessoires_italiaans { get; set; }
        [XmlElement(ElementName = "accessoires_grieks")]
        public string Accessoires_grieks { get; set; }
        [XmlElement(ElementName = "accessoires_russisch")]
        public string Accessoires_russisch { get; set; }
        [XmlElement(ElementName = "accessoires_roemeens")]
        public string Accessoires_roemeens { get; set; }
        [XmlElement(ElementName = "accessoires_hongaars")]
        public string Accessoires_hongaars { get; set; }
        [XmlElement(ElementName = "accessoires_pools")]
        public string Accessoires_pools { get; set; }
        [XmlElement(ElementName = "accessoires_tsjechisch")]
        public string Accessoires_tsjechisch { get; set; }
        [XmlElement(ElementName = "accessoires_bulgaars")]
        public string Accessoires_bulgaars { get; set; }
        [XmlElement(ElementName = "accessoires_kroatisch")]
        public string Accessoires_kroatisch { get; set; }
        [XmlElement(ElementName = "accessoires_deens")]
        public string Accessoires_deens { get; set; }
        [XmlElement(ElementName = "accessoires_slowaaks")]
        public string Accessoires_slowaaks { get; set; }
        [XmlElement(ElementName = "accessoires_turks")]
        public string Accessoires_turks { get; set; }
        [XmlElement(ElementName = "accessoires_fins")]
        public string Accessoires_fins { get; set; }
        [XmlElement(ElementName = "accessoires_zweeds")]
        public string Accessoires_zweeds { get; set; }
        [XmlElement(ElementName = "accessoires_noors")]
        public string Accessoires_noors { get; set; }
        [XmlElement(ElementName = "accessoiregroepen")]
        public Accessoiregroepen Accessoiregroepen { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_engels")]
        public string Accessoiregroepen_engels { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_duits")]
        public string Accessoiregroepen_duits { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_frans")]
        public string Accessoiregroepen_frans { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_spaans")]
        public string Accessoiregroepen_spaans { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_portugees")]
        public string Accessoiregroepen_portugees { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_italiaans")]
        public string Accessoiregroepen_italiaans { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_grieks")]
        public string Accessoiregroepen_grieks { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_russisch")]
        public string Accessoiregroepen_russisch { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_roemeens")]
        public string Accessoiregroepen_roemeens { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_hongaars")]
        public string Accessoiregroepen_hongaars { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_pools")]
        public string Accessoiregroepen_pools { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_tsjechisch")]
        public string Accessoiregroepen_tsjechisch { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_bulgaars")]
        public string Accessoiregroepen_bulgaars { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_kroatisch")]
        public string Accessoiregroepen_kroatisch { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_deens")]
        public string Accessoiregroepen_deens { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_slowaaks")]
        public string Accessoiregroepen_slowaaks { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_turks")]
        public string Accessoiregroepen_turks { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_fins")]
        public string Accessoiregroepen_fins { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_zweeds")]
        public string Accessoiregroepen_zweeds { get; set; }
        [XmlElement(ElementName = "accessoiregroepen_noors")]
        public string Accessoiregroepen_noors { get; set; }
        [XmlElement(ElementName = "zoekaccessoires")]
        public Zoekaccessoires Zoekaccessoires { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_engels")]
        public Zoekaccessoires_engels Zoekaccessoires_engels { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_duits")]
        public Zoekaccessoires_duits Zoekaccessoires_duits { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_frans")]
        public Zoekaccessoires_frans Zoekaccessoires_frans { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_spaans")]
        public Zoekaccessoires_spaans Zoekaccessoires_spaans { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_portugees")]
        public Zoekaccessoires_portugees Zoekaccessoires_portugees { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_italiaans")]
        public Zoekaccessoires_italiaans Zoekaccessoires_italiaans { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_grieks")]
        public Zoekaccessoires_grieks Zoekaccessoires_grieks { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_russisch")]
        public Zoekaccessoires_russisch Zoekaccessoires_russisch { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_roemeens")]
        public Zoekaccessoires_roemeens Zoekaccessoires_roemeens { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_hongaars")]
        public Zoekaccessoires_hongaars Zoekaccessoires_hongaars { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_pools")]
        public Zoekaccessoires_pools Zoekaccessoires_pools { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_tsjechisch")]
        public Zoekaccessoires_tsjechisch Zoekaccessoires_tsjechisch { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_bulgaars")]
        public Zoekaccessoires_bulgaars Zoekaccessoires_bulgaars { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_kroatisch")]
        public Zoekaccessoires_kroatisch Zoekaccessoires_kroatisch { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_deens")]
        public Zoekaccessoires_deens Zoekaccessoires_deens { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_slowaaks")]
        public Zoekaccessoires_slowaaks Zoekaccessoires_slowaaks { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_turks")]
        public Zoekaccessoires_turks Zoekaccessoires_turks { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_fins")]
        public string Zoekaccessoires_fins { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_zweeds")]
        public string Zoekaccessoires_zweeds { get; set; }
        [XmlElement(ElementName = "zoekaccessoires_noors")]
        public string Zoekaccessoires_noors { get; set; }
        [XmlElement(ElementName = "schade")]
        public Schade Schade { get; set; }
        [XmlElement(ElementName = "vgs")]
        public Vgs Vgs { get; set; }
        [XmlElement(ElementName = "autotelex")]
        public Autotelex Autotelex { get; set; }
        [XmlElement(ElementName = "aantal_afbeeldingen")]
        public string Aantal_afbeeldingen { get; set; }
        [XmlElement(ElementName = "aantal_panoramas")]
        public string Aantal_panoramas { get; set; }
        [XmlElement(ElementName = "afbeeldingen")]
        public Afbeeldingen Afbeeldingen { get; set; }
        [XmlElement(ElementName = "overlays")]
        public string Overlays { get; set; }
        [XmlElement(ElementName = "panoramas")]
        public string Panoramas { get; set; }
        [XmlElement(ElementName = "videos")]
        public string Videos { get; set; }
        [XmlElement(ElementName = "documenten")]
        public string Documenten { get; set; }
       
    }

    [XmlRoot(ElementName = "inhoud")]
    public class Inhoud
    {
        [XmlElement(ElementName = "element")]
        public List<string> Element { get; set; }
    }

    [XmlRoot(ElementName = "bovag_garantie")]
    public class Bovag_garantie
    {
        [XmlAttribute(AttributeName = "maanden")]
        public string Maanden { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "garanties")]
    public class Garanties
    {
        [XmlElement(ElementName = "merkgarantie")]
        public string Merkgarantie { get; set; }
        [XmlElement(ElementName = "bovag_garantie")]
        public Bovag_garantie Bovag_garantie { get; set; }
        [XmlElement(ElementName = "bovag_40puntencheck")]
        public string Bovag_40puntencheck { get; set; }
        [XmlElement(ElementName = "bovag_afleverbeurt")]
        public string Bovag_afleverbeurt { get; set; }
        [XmlElement(ElementName = "vetos_garantie")]
        public string Vetos_garantie { get; set; }
        [XmlElement(ElementName = "pca_top_occasion")]
        public string Pca_top_occasion { get; set; }
        [XmlElement(ElementName = "bosch_car_service_best_car_selection")]
        public string Bosch_car_service_best_car_selection { get; set; }
        [XmlElement(ElementName = "vakgarant_premium_occasion")]
        public string Vakgarant_premium_occasion { get; set; }
        [XmlElement(ElementName = "autotrust_garantie")]
        public string Autotrust_garantie { get; set; }
        [XmlElement(ElementName = "vwe_occasion_garant_plan")]
        public string Vwe_occasion_garant_plan { get; set; }
        [XmlElement(ElementName = "car_garantie")]
        public string Car_garantie { get; set; }
        [XmlElement(ElementName = "mbsg_garantie")]
        public string Mbsg_garantie { get; set; }
        [XmlElement(ElementName = "dealergarantie")]
        public string Dealergarantie { get; set; }
        [XmlElement(ElementName = "aanvullende_garantie")]
        public string Aanvullende_garantie { get; set; }
        [XmlElement(ElementName = "opmerkingen")]
        public string Opmerkingen { get; set; }
    }

    [XmlRoot(ElementName = "afleverpakket")]
    public class Afleverpakket
    {
        [XmlElement(ElementName = "naam")]
        public string Naam { get; set; }
        [XmlElement(ElementName = "omschrijving")]
        public string Omschrijving { get; set; }
        [XmlElement(ElementName = "inhoud")]
        public Inhoud Inhoud { get; set; }
        [XmlElement(ElementName = "prijs_ex")]
        public string Prijs_ex { get; set; }
        [XmlElement(ElementName = "prijs_in")]
        public string Prijs_in { get; set; }
        [XmlElement(ElementName = "prijs_ex_rel")]
        public string Prijs_ex_rel { get; set; }
        [XmlElement(ElementName = "prijs_in_rel")]
        public string Prijs_in_rel { get; set; }
        [XmlElement(ElementName = "accessoires")]
        public string Accessoires { get; set; }
        [XmlElement(ElementName = "apk_bij_aflevering")]
        public string Apk_bij_aflevering { get; set; }
        [XmlElement(ElementName = "garantie_opmerking")]
        public string Garantie_opmerking { get; set; }
        [XmlElement(ElementName = "garanties")]
        public Garanties Garanties { get; set; }
        [XmlAttribute(AttributeName = "volgorde")]
        public string Volgorde { get; set; }
    }

    [XmlRoot(ElementName = "afleverpakketten")]
    public class Afleverpakketten
    {
        [XmlElement(ElementName = "afleverpakket")]
        public Afleverpakket Afleverpakket { get; set; }
    }

    [XmlRoot(ElementName = "zoekaccessoires_kroatisch")]
    public class Zoekaccessoires_kroatisch
    {
        [XmlElement(ElementName = "accessoire")]
        public string Accessoire { get; set; }
    }

    [XmlRoot(ElementName = "voorraad")]
    public class Voorraad
    {
        [XmlElement(ElementName = "voertuig")]
        public List<Voertuig> Voertuig { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "noNamespaceSchemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string NoNamespaceSchemaLocation { get; set; }
        [XmlAttribute(AttributeName = "date")]
        public string Date { get; set; }
        [XmlAttribute(AttributeName = "versie")]
        public string Versie { get; set; }
        public string FileName { get; set; }
    }

}
