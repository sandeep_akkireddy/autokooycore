﻿using FusionCore.Core.Extensions;
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AutoKooyCore.XMLHelpers
{
    public class FetchXMLData
    {
        public static List<XMLViewModel> GetXmlData()
        {
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile("appsettings.json", false);

            var root = configurationBuilder.Build();
            FileSettings fileSettings = root.GetSection("FileSettings").Get<FileSettings>();
            MainSettings mainSettings = root.GetSection("MainSettings").Get<MainSettings>();

            List<XMLViewModel> occasions = new List<XMLViewModel>();

            var filepath = fileSettings.XmlFiles;
            var files = Directory.EnumerateFiles(filepath, "*.xml");
            if (files != null && files.Any())
            {
                foreach (string file in files)
                {
                    XmlSerializer mySerializer = new XmlSerializer(typeof(Voorraad));
                    using (StreamReader streamReader = new StreamReader(file))
                    {
                        var data = (Voorraad)mySerializer.Deserialize(streamReader);
                        if (data != null && data.Voertuig != null && data.Voertuig.Any())
                        {
                            foreach (var car in data.Voertuig)
                            {
                                if (car.Afbeeldingen != null && car.Afbeeldingen.Afbeelding != null && car.Afbeeldingen.Afbeelding.Any())
                                {
                                    car.Afbeeldingen.Afbeelding.ForEach(x =>
                                    {
                                        var imagename = x.Bestandsnaam;
                                        x.Bestandsnaam = mainSettings.WebsiteUrl + fileSettings.XmlFileImagesRelativePath + imagename;
                                        x.Nr_nr = Convert.ToInt32(x.Nr);
                                    });
                                }
                                if (!string.IsNullOrEmpty(car.Invoerdatum))
                                {
                                    car.Invoerdatum_dt = DateExtensions.ParseDateTime(new string[] { "dd-MM-yyyy"}, car.Invoerdatum);
                                }
                                if (!string.IsNullOrEmpty(car.Titel))
                                {
                                    car.Titel_url = UrlHelper.GetFrienlyUrl(car.Titel);
                                }
                                data.FileName = Path.GetFileNameWithoutExtension(file);
                                
                                XMLViewModel xvm = new XMLViewModel();
                                xvm.Date = data.Date;
                                xvm.FileName = Path.GetFileNameWithoutExtension(file);
                                xvm.NoNamespaceSchemaLocation = data.NoNamespaceSchemaLocation;
                                xvm.Versie = data.Versie;
                                xvm.Voertuig = car;
                                xvm.Xsi = data.Xsi;
                                occasions.Add(xvm);
                            }
                        }
                    }
                }


            }

            return occasions;
        }
        public static XMLViewModel GetSingleXmlData(string filename,string vehicleno)
        {
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile("appsettings.json", false);

            var root = configurationBuilder.Build();
            FileSettings fileSettings = root.GetSection("FileSettings").Get<FileSettings>();
            MainSettings mainSettings = root.GetSection("MainSettings").Get<MainSettings>();

            XMLViewModel occasions = new XMLViewModel();

            var filepath = fileSettings.XmlFiles + filename + ".xml";
            if (File.Exists(filepath))
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(Voorraad));
                using (StreamReader streamReader = new StreamReader(filepath))
                {
                    var data = (Voorraad)mySerializer.Deserialize(streamReader);
                    if (data != null && data.Voertuig != null && data.Voertuig.Any())
                    {
                        var car = data.Voertuig.FirstOrDefault(x=>x.Voertuignr==vehicleno);
                        if (car != null)
                        {
                            if (car.Afbeeldingen != null && car.Afbeeldingen.Afbeelding != null && car.Afbeeldingen.Afbeelding.Any())
                            {
                                car.Afbeeldingen.Afbeelding.ForEach(x =>
                                {
                                    var imagename = x.Bestandsnaam;
                                    x.Bestandsnaam = mainSettings.WebsiteUrl + fileSettings.XmlFileImagesRelativePath + imagename;
                                    x.Nr_nr = Convert.ToInt32(x.Nr);
                                });
                            }
                            if (!string.IsNullOrEmpty(car.Invoerdatum))
                            {
                                car.Invoerdatum_dt = DateExtensions.ParseDateTime(new string[] {  "dd-MM-yyyy" }, car.Invoerdatum);
                            }
                            if (!string.IsNullOrEmpty(car.Titel))
                            {
                                car.Titel_url = UrlHelper.GetFrienlyUrl(car.Titel);
                            }
                            data.FileName = Path.GetFileNameWithoutExtension(filepath);
                        }

                        XMLViewModel xvm = new XMLViewModel();
                        xvm.Date = data.Date;
                        xvm.FileName = Path.GetFileNameWithoutExtension(filepath);
                        xvm.NoNamespaceSchemaLocation = data.NoNamespaceSchemaLocation;
                        xvm.Versie = data.Versie;
                        xvm.Voertuig = car;
                        xvm.Xsi = data.Xsi;
                        occasions = xvm;
                    }
                }
            }
            return occasions;
        }
    }
}
