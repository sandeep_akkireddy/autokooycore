﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoKooyCore.XMLHelpers
{
    public class XMLViewModel
    {
        public Voertuig Voertuig { get; set; }
   
        public string Xsi { get; set; }
      
        public string NoNamespaceSchemaLocation { get; set; }
      
        public string Date { get; set; }
       
        public string Versie { get; set; }
        public string FileName { get; set; }
    }
}
