﻿using FusionCore.Modules.FundsManager.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoKooyCore.ViewComponents
{
    public class BannerSlider : ViewComponent
    {
        private readonly IBannerService _bannerService;
        public BannerSlider(IBannerService bannerService)
        {
            _bannerService = bannerService;
        }
        public IViewComponentResult Invoke()
        {
            return View(_bannerService.GetActiveBanner());
        }
    }
}
