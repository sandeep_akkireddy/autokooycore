﻿using AutoKooyCore.XMLHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoKooyCore.Models
{
    public class OccassionsViewModel
    {
        public List<XMLViewModel> Occasions { get; set; }
        public List<XMLViewModel> MoreOccasions { get; set; }
        public XMLViewModel OccasionsDetail { get; set; }
        public bool ShowNext { get; set; }
        public bool ShowPrev { get; set; }
        public string NextUrl { get; set; }
        public string PrevUrl { get; set; }
        public string NextTitle { get; set; }
        public string PrevTitle { get; set; }
    }
    public class BouwjaarViewModel
    {
        public int Jaar { get; set; }
        public int Aantal { get; set; }
    }
    public class BrandViewModel
    {
        public string BrandName { get; set; }
        public int Aantal { get; set; }
    }
}
