﻿using AutoKooyCore.XMLHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoKooyCore.Models
{
    public class HomeViewModel
    {
        public List<XMLViewModel> occasions { get; set; }
        public XMLViewModel WeeklyOccasion { get; set; }
    }
}
