﻿using AutoKooyCore.XMLHelpers;
using FusionCore.Modules.FundsManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoKooyCore.Models
{
    public class PaginaViewModel
    {
        public PagePersistModel Page { get; set; }
        public List<XMLViewModel> MoreOccasions { get; set; }
    }
}
