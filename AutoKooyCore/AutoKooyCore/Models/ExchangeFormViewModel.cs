﻿using AutoKooyCore.XMLHelpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AutoKooyCore.Models
{
    public class ExchangeFormViewModel
    {
        [Required]
        public string name { get; set; }
        [Required]
        public string lastname { get; set; }
        [Required]
        public string telephone { get; set; }
        [Required]
        public string email { get; set; }
        public string interestedcar { get; set; }
        [Required]
        public string brandname { get; set; }
        [Required]
        public string modelname { get; set; }
        [Required]
        public string licenseplate { get; set; }
        [Required]
        public string mileage { get; set; }
        [Required]
        public string damage { get; set; }
        [Required]
        public string message { get; set; }
        public IFormFile carphoto { get; set; }

        public List<string> autotelex { get; set; }

    }
}
