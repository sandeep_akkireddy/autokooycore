﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoKooyCore.Models;
using AutoKooyCore.XMLHelpers;
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Update.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using AutoKooyCore.Helpers;

namespace AutoKooyCore.Controllers
{
    public class OccasionsController : Controller
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        public OccasionsController(ILogger<OccasionsController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }
        public IActionResult Index(string kind)
        {
            OccassionsViewModel ovm = new OccassionsViewModel();
            try
            {
                var allCars = FetchXMLData.GetXmlData().OrderBy(x => x.Voertuig.Titel).ToList();
                if (!string.IsNullOrEmpty(kind))
                {
                    switch (kind)
                    {
                        case "tot-2000":
                            allCars =
                                allCars.Where(x => StringHelpers.IsNumeric(x.Voertuig.Verkoopprijs_particulier.Text) && Convert.ToInt32(x.Voertuig.Verkoopprijs_particulier.Text) <= 2000).OrderBy(x => x.Voertuig.Titel).ToList();
                            break;
                        case "laatste":
                            allCars =
                                allCars.OrderByDescending(x => x.Voertuig.Invoerdatum_dt).ThenBy(x => x.Voertuig.Titel).ToList();
                            break;
                    }
                }
                ViewBag.Bouwjaren =
                    allCars.Where(c => c.Voertuig != null).Select(c => c.Voertuig.Bouwjaar)
                        .GroupBy(c => c)
                        .Select(c => new BouwjaarViewModel { Jaar = int.Parse(c.Key), Aantal = c.Count() })
                        .OrderByDescending(c => c.Jaar)
                        .ToList();
                ViewBag.Brands =
                  allCars.Where(c => c.Voertuig != null).Select(c => c.Voertuig.Merk)
                      .GroupBy(c => c)
                      .Select(c => new BrandViewModel { BrandName = c.Key, Aantal = c.Count() })
                      .OrderBy(c => c.BrandName)
                      .ToList();

                ovm.Occasions = allCars;
                ovm.MoreOccasions = allCars.ToArray().PickRandom(4);
            }
            catch (Exception ex)
            {
                _logger.LogError("Occasion Index Exception");
                _logger.LogError($"Message : {ex.Message}");
                _logger.LogError($"StackTrace : {ex.StackTrace}");
                if (ex.InnerException != null)
                    _logger.LogError($"InnerException : {ex.InnerException.Message}");
            }
            return View(ovm);
        }
        public IActionResult Detail(string filename, string vehicleno)
        {
            OccassionsViewModel ovm = new OccassionsViewModel();
            try
            {
                var allCars = FetchXMLData.GetXmlData().OrderBy(x => x.Voertuig.Titel).ToList();
                var car = allCars.FirstOrDefault(x => x.FileName == filename && x.Voertuig.Voertuignr == vehicleno); //FetchXMLData.GetSingleXmlData(id);

                var currentindex = allCars.FindIndex(x => x.FileName == filename && x.Voertuig.Voertuignr == vehicleno);
                var allcount = allCars.Count;
                bool showprev = true, shownext = true;
                string nexturl = "", prevurl = "", nexttitle = "", prevtitle = "";
                int nextindex = (currentindex + 1), previndex = (currentindex - 1);

                if (nextindex <= 1)
                {
                    showprev = false;
                }
                else
                {
                    var previtem = allCars[previndex];
                    prevurl = "/occasions/" + previtem.Voertuig.Titel_url + "/" + previtem.FileName + "/" + previtem.Voertuig.Voertuignr;
                    prevtitle = previtem.Voertuig.Titel;
                }

                if (nextindex >= allcount)
                {
                    shownext = false;
                }
                else
                {
                    var nextitem = allCars[nextindex];
                    nexturl = "/occasions/" + nextitem.Voertuig.Titel_url + "/" + nextitem.FileName + "/" + nextitem.Voertuig.Voertuignr;
                    nexttitle = nextitem.Voertuig.Titel;
                }


                ovm.OccasionsDetail = car;
                ovm.NextUrl = nexturl;
                ovm.PrevUrl = prevurl;
                ovm.ShowNext = shownext;
                ovm.ShowPrev = showprev;
                ovm.NextTitle = nexttitle;
                ovm.PrevTitle = prevtitle;
                //x.FileName != filename &&
                var more_occs = allCars.Where(x => x.Voertuig.Voertuignr != vehicleno).ToArray();
                if (more_occs != null && more_occs.Any())
                {
                    ovm.MoreOccasions = more_occs.PickRandom(4);
                }

                if (car != null && car.Voertuig != null)
                {
                    MainSettings mainSettings = _configuration.GetSection("MainSettings").Get<MainSettings>();

                    var currenturl = Uri.EscapeUriString(mainSettings.WebsiteUrl + HttpContext.Request.Path.ToUriComponent().Substring(1));

                    var sharetitle = StripHtml.RemoveSpecialcharswithspace(car.Voertuig.Titel);
                    var shareurl = Uri.EscapeUriString(currenturl);
                    var sharedesc = StripHtml.RemoveSpecialcharswithspace(StripHtml.StripHtmlTags(car.Voertuig.Opmerkingen));
                    var shareimage = Uri.EscapeUriString(car.Voertuig.Afbeeldingen?.Afbeelding.OrderBy(x => x.Nr_nr)?.FirstOrDefault()?.Bestandsnaam);

                    ViewBag.ShareTitle = sharetitle;
                    ViewBag.ShareURL = shareurl;
                    ViewBag.ShareDescription = sharedesc;
                    ViewBag.ShareImage = shareimage;

                    ViewBag.facebookurl = "https://www.facebook.com/sharer/sharer.php?u=" + shareurl + "&title=" + sharetitle + "&picture=" + shareimage;

                    ViewBag.twitterurl = "https://twitter.com/share?url=" + shareurl;

                    ViewBag.linkedinurl = "https://www.linkedin.com/shareArticle?mini=true&url=" + shareurl + "&title=" + sharetitle;

                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Occasion Detail Exception");
                _logger.LogError($"Message : {ex.Message}");
                _logger.LogError($"StackTrace : {ex.StackTrace}");
                if (ex.InnerException != null)
                    _logger.LogError($"InnerException : {ex.InnerException.Message}");
            }
            return View(ovm);
        }
    }
}
