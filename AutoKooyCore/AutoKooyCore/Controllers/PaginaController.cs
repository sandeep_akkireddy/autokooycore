﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FusionCore.Modules.FundsManager.Interfaces;
using Microsoft.AspNetCore.Mvc;
using FusionCore.Modules.FundsManager.Models;
using FusionCore.Core.Settings;
using AutoKooyCore.XMLHelpers;
using Microsoft.Extensions.Configuration;
using AutoKooyCore.Models;
using AutoKooyCore.Helpers;
using Microsoft.Extensions.Logging;

namespace AutoKooyCore.Controllers
{
    public class PaginaController : Controller
    {
        IPageService _pageService;
        ILogger _logger;

        public IActionResult Index()
        {
            PaginaViewModel pvm = new PaginaViewModel();
            try
            {
                _pageService = (IPageService)HttpContext.RequestServices.GetService(typeof(IPageService));
                _logger = (ILogger<PaginaController>)HttpContext.RequestServices.GetService(typeof(ILogger<PaginaController>));

                var path = HttpContext.Request.Path;
                var url = path.HasValue ? path
                    .Value.Replace("/Pagina/Index", "") : "";
                PagePersistModel model = _pageService.GetPageByUrl(url);

                var configurationBuilder = new ConfigurationBuilder();
                configurationBuilder.AddJsonFile("appsettings.json", false);

                var root = configurationBuilder.Build();

                MainSettings mainSettings = root.GetSection("MainSettings").Get<MainSettings>();

                var currenturl = Uri.EscapeUriString(mainSettings.WebsiteUrl + HttpContext.Request.Path.ToUriComponent().Substring(1));

                var sharetitle = StripHtml.RemoveSpecialcharswithspace(model.Title);
                var shareurl = Uri.EscapeUriString(currenturl);
                var sharedesc = StripHtml.RemoveSpecialcharswithspace(StripHtml.StripHtmlTags(model.Content));


                ViewBag.ShareTitle = sharetitle;
                ViewBag.ShareURL = shareurl;
                ViewBag.ShareDescription = sharedesc;


                ViewBag.facebookurl = "https://www.facebook.com/sharer/sharer.php?u=" + shareurl + "&title=" + sharetitle;

                ViewBag.twitterurl = "https://twitter.com/share?url=" + shareurl;

                ViewBag.linkedinurl = "https://www.linkedin.com/shareArticle?mini=true&url=" + shareurl + "&title=" + sharetitle;

                pvm.Page = model;
                pvm.MoreOccasions = FetchXMLData.GetXmlData().OrderBy(x => x.Voertuig.Titel).ToArray().PickRandom(4);
            }
            catch (Exception ex)
            {
                _logger.LogError("Pagina Exception");
                _logger.LogError($"Message : {ex.Message}");
                _logger.LogError($"StackTrace : {ex.StackTrace}");
                if (ex.InnerException != null)
                    _logger.LogError($"InnerException : {ex.InnerException.Message}");
            }
            return View(pvm);
        }
    }
}
