﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FusionCore.Core.Settings;
using FusionCore.Services.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AutoKooyCore.Controllers
{
    public class FileController : Controller
    {
        private readonly IFileService _fileService;
        private readonly IConfiguration _configuration;

        public FileController(IFileService fileService, IConfiguration configuration)
        {
            _fileService = fileService;
            _configuration = configuration;
        }
        public IActionResult Get(int id, string type)
        {
            var file = _fileService.GetFile(id);
            if (file != null)
            {
                if (file.FileLocal != null)
                {
                    FileSettings fileSettings = _configuration.GetSection("FileSettings").Get<FileSettings>();
                    MainSettings mainSettings = _configuration.GetSection("MainSettings").Get<MainSettings>();

                    string imagepath = "";
                    if (type != null)
                    {
                        type = type.ToLower().Trim();
                        if (type.Contains("team"))
                        {
                            imagepath = (fileSettings.TeamsFiles + file.FileLocal);
                        }
                        else if (type.Contains("news"))
                        {
                            imagepath = (fileSettings.NewsFiles + file.FileLocal);
                        }
                        else if (type.Contains("splashimage") || type.Contains("thumbnailimage"))
                        {
                            imagepath = (fileSettings.FundFiles + file.FileLocal);
                        }
                        else if (type.Contains("bannerfileid"))
                        { 
                            imagepath = (fileSettings.BannerFiles + file.FileLocal); 
                        }
                        else if (type.Contains("support"))
                        {
                            imagepath = (fileSettings.SupportFiles + file.FileLocal);
                        }
                    }
                    byte[] binaryfile = System.IO.File.ReadAllBytes(imagepath);
                    return new FileStreamResult(new MemoryStream(binaryfile), file.ContentType);
                }
                else if (file.FileBinary != null)
                {
                    return new FileStreamResult(new MemoryStream(file.FileBinary), file.ContentType);
                }

            }

            return Content("");
        }
    }
}