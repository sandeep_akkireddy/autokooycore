﻿using AutoKooyCore.Models;
using AutoKooyCore.XMLHelpers;
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FusionCore.Core.Extensions;
using System.Reflection;

namespace AutoKooyCore.Controllers
{
    public class ExchangeFormController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        public ExchangeFormController(IConfiguration configuration, ILogger<ExchangeFormController> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public IActionResult Index()
        {
            var allCars = FetchXMLData.GetXmlData().OrderBy(x => x.Voertuig.Titel).ToList();
            var autotelex = allCars.Where(x => x.Voertuig != null && x.Voertuig.Autotelex != null).Select(x => x.Voertuig.Autotelex).Select(x => new
            {
                carsinfo = x.Merk + " " + x.Model + " " + x.Uitvoering
            }).Select(x => x.carsinfo).Distinct().ToList();
            HttpContext.Session.SetComplexData("Autotelex", autotelex);

            ExchangeFormViewModel model = new ExchangeFormViewModel();
            model.autotelex = autotelex;
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(IFormCollection collection, ExchangeFormViewModel model)
        {
            var recaptcha = _configuration.GetSection("Recaptcha").Get<Recaptcha>();
            var emailsettings = _configuration.GetSection("EmailSettings").Get<EmailSettings>();
            var captcharesponse = collection["g-recaptcha-response"];
            if (ModelState.IsValid && RecaptchaHelper.IsReCaptchValid(captcharesponse, recaptcha.recaptchaPrivatekey))
            {
                try
                {
                    string body = "U heeft een Inruilformulier ontvangen met de volgende gegevens: <br/>";
                    foreach (PropertyInfo prop in model.GetType().GetProperties())
                    {
                        if (prop.Name != "carphoto" && prop.Name != "autotelex")
                        {
                            body += prop.Name + " : " + prop.GetValue(model) + "<br/>";
                        }
                    }
                    //foreach (var item in collection)
                    //{
                    //    var key = item.Key;
                    //    if (key != "g-recaptcha-response" && key != "carphoto" && key != "__RequestVerificationToken")
                    //    {
                    //        body += key + " : " + collection[key] + "<br/>";
                    //    }
                    //}
                    var mailFunction = new MailFunction(emailsettings);
                    if (model.carphoto != null)
                    {
                        MemoryStream memoryStream = new MemoryStream();
                        model.carphoto.CopyTo(memoryStream);
                        memoryStream.Position = 0;
                        string filename = Guid.NewGuid().ToString() + model.carphoto.FileName;
                        mailFunction.AttachmentsStreams.Add(filename, memoryStream);
                    }
                    mailFunction.EmailFrom = model.email;
                    mailFunction.EmailFromName = model.name;
                    mailFunction.EmailTo = emailsettings.AutokooyEmail;
                    mailFunction.EmailToName = "Autokooy";
                    mailFunction.Body = body;
                    mailFunction.IsHtml = true;
                    mailFunction.Subject = "Inruilformulier";
                    mailFunction.SendEmail();


                    model = new ExchangeFormViewModel();
                    model.autotelex = HttpContext.Session.GetComplexData<List<string>>("Autotelex");

                    ViewBag.formsent = "success";
                    ModelState.Clear();
                }
                catch (Exception er)
                {
                    _logger.LogError("Inruilformulier", er);
                }
            }
            else
            {

            }
            return View(model);
        }
    }
}
