﻿
using FusionCore.Core.Helpers;
using FusionCore.Core.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using AutoKooyCore.Models;
using System;

namespace AutoKooyCore.Controllers
{
    public class CommonController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        public CommonController(IConfiguration configuration, ILogger<CommonController> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        [AllowAnonymous]
        public IActionResult Error()
        {
            var exceptionDetails = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            if (exceptionDetails != null)
                ViewBag.ErrorPageError = exceptionDetails.Error != null ? exceptionDetails.Error.Message : "";
            return View("Error", new ErrorViewModel());
        }
        [HttpPost]
        public IActionResult Contact(IFormCollection collection)
        {
            var recaptcha = _configuration.GetSection("Recaptcha").Get<Recaptcha>();
            var emailsettings = _configuration.GetSection("EmailSettings").Get<EmailSettings>();
            var captcharesponse = collection["g-recaptcha-response"];

            if (RecaptchaHelper.IsReCaptchValid(captcharesponse, recaptcha.recaptchaPrivatekey))
            {
                if (!string.IsNullOrEmpty(collection["subject"]) &&
                    !string.IsNullOrEmpty(collection["name"]) &&
                    !string.IsNullOrEmpty(collection["email"]) &&
                    !string.IsNullOrEmpty(collection["bericht"]))
                {
                    try
                    {
                        string body = "U heeft een contactformulier ontvangen met de volgende gegevens: <br/>";
                        foreach (var item in collection)
                        {
                            var key = item.Key;
                            if (key != "g-recaptcha-response")
                            {
                                body += key + " : " + collection[key] + "<br/>";
                            }
                        }
                        var mailFunction = new MailFunction(emailsettings);
                        mailFunction.EmailFrom = collection["email"];
                        mailFunction.EmailFromName = collection["name"];
                        mailFunction.EmailTo = emailsettings.AutokooyEmail;
                        mailFunction.EmailToName = "Autokooy";
                        mailFunction.Body = body;
                        mailFunction.IsHtml = true;
                        mailFunction.Subject = "Contactformulier";
                        mailFunction.SendEmail();
                        return Json(true);

                    }
                    catch (Exception er)
                    {
                        _logger.LogError("Contactformulier", er);
                    }
                }
            }
            return Json(false);
        }
        [HttpPost]
        public IActionResult TestDrive(IFormCollection collection)
        {
            var emailsettings = _configuration.GetSection("EmailSettings").Get<EmailSettings>();


            if (!string.IsNullOrEmpty(collection["bericht"]) &&
                !string.IsNullOrEmpty(collection["name"]) &&
                !string.IsNullOrEmpty(collection["email"]) &&
                !string.IsNullOrEmpty(collection["tel"]) &&
                !string.IsNullOrEmpty(collection["voorkeursdag"]) &&
                !string.IsNullOrEmpty(collection["dagdeel"]))
            {
                try
                {
                    string body = "Er is een aanvraag voor een proefrit binnen gekomen voor de volgende auto: <br/><strong>" + collection["hiddentestdrivecarname"] + "</strong><br/><br/>";
                    foreach (var item in collection)
                    {
                        var key = item.Key;
                        if (key != "hiddentestdrivecarname")
                            body += key + " : " + collection[key] + "<br/>";

                    }
                    var mailFunction = new MailFunction(emailsettings);
                    mailFunction.EmailFrom = collection["email"];
                    mailFunction.EmailFromName = collection["name"];
                    mailFunction.EmailTo = emailsettings.AutokooyEmail;
                    mailFunction.EmailToName = "Autokooy";
                    mailFunction.Body = body;
                    mailFunction.IsHtml = true;
                    mailFunction.Subject = "Proefrit aanvraag";
                    mailFunction.SendEmail();
                    return Json(true);

                }
                catch (Exception er)
                {
                    _logger.LogError("Proefrit aanvraag", er);
                }
            }

            return Json(false);
        }

    }
}