﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoKooyCore.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using AutoMapper;
using System.IO;
using System.Xml.Serialization;
using AutoKooyCore.XMLHelpers;
using FusionCore.Core.Settings;
using FusionCore.Core.Extensions;
using FusionCore.Services.Car;
using FusionCore.Models.Frontend.Cars;
using FusionCore.Migrations;
using Microsoft.AspNetCore.Http;
using FusionCore.Core.Helpers;

namespace AutoKooyCore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger _logger;
        private readonly ICarsService _carsService;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public HomeController(IConfiguration configuration, ILogger<HomeController> logger, ICarsService carService, IMapper mapper)
        {
            _logger = logger;
            _carsService = carService;
            _mapper = mapper;
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            HomeViewModel hvm = new HomeViewModel();
            try
            {
                var occasions = FetchXMLData.GetXmlData();
                if (occasions != null && occasions.Any())
                {
                    hvm.occasions = occasions.OrderByDescending(x => x.Voertuig.Invoerdatum_dt).ToList();
                    var random = new Random();
                    int index = random.Next(1, occasions.Count);

                    var weekcar = _mapper.Map<WeeklyCarModel>(_carsService.GetWeeklyCar());
                    if (weekcar != null)
                    {
                        var carinfo = occasions.FirstOrDefault(x => x.FileName == weekcar.FileName && x.Voertuig.Voertuignr == weekcar.VehicleNumber);
                        if (carinfo != null && weekcar.WeekExpiry > DateTime.Now)
                        {
                            hvm.WeeklyOccasion = carinfo;
                        }
                        else
                        {
                            var _carinfo = occasions[index - 1];
                            hvm.WeeklyOccasion = _carinfo;
                            InsertRandomCar(_carinfo);
                        }
                    }
                    else
                    {
                        var _carinfo = occasions[index - 1];
                        hvm.WeeklyOccasion = _carinfo;
                        InsertRandomCar(_carinfo);
                    }


                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Home Exception");
                _logger.LogError($"Message : {ex.Message}");
                _logger.LogError($"StackTrace : {ex.StackTrace}");
                if (ex.InnerException != null)
                    _logger.LogError($"InnerException : {ex.InnerException.Message}");
            }
            return View(hvm);
        }
        void InsertRandomCar(XMLViewModel _carinfo)
        {

            var today = DateTime.Now;
            var nextsaturday = today.AddDays(DayOfWeek.Saturday - today.DayOfWeek);
            var expirydate = new DateTime(nextsaturday.Year, nextsaturday.Month, nextsaturday.Day, 23, 59, 59);
            if (today.DayOfWeek == DayOfWeek.Saturday)
            {
                expirydate = expirydate.AddDays(7);
            }
            WeeklyCarModel model = new WeeklyCarModel();
            model.VehicleNumber = _carinfo.Voertuig.Voertuignr;
            model.WeekExpiry = expirydate;
            model.FileName = _carinfo.FileName;

            _carsService.AddWeeklyCar(model);
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult Appointment(IFormCollection collection)
        {
            var recaptcha = _configuration.GetSection("Recaptcha").Get<Recaptcha>();
            var emailsettings = _configuration.GetSection("EmailSettings").Get<EmailSettings>();
            var captcharesponse = collection["g-recaptcha-response"];

            if (RecaptchaHelper.IsReCaptchValid(captcharesponse, recaptcha.recaptchaPrivatekey))
            {
                if (!string.IsNullOrEmpty(collection["onderwerp"]) &&
                    !string.IsNullOrEmpty(collection["name"]) &&
                    !string.IsNullOrEmpty(collection["email"]) &&
                    !string.IsNullOrEmpty(collection["bericht"]))
                {
                    try
                    {
                        string body = "U heeft een APK ontvangen met de volgende gegevens: <br/>";
                        foreach (var item in collection)
                        {
                            var key = item.Key;
                            if (key != "g-recaptcha-response")
                            {
                                body += key + " : " + collection[key] + "<br/>";
                            }
                        }
                        var mailFunction = new MailFunction(emailsettings);
                        mailFunction.EmailFrom = collection["email"];
                        mailFunction.EmailFromName = collection["name"];
                        mailFunction.EmailTo = emailsettings.AutokooyEmail;
                        mailFunction.EmailToName = "Autokooy";
                        mailFunction.Body = body;
                        mailFunction.IsHtml = true;
                        mailFunction.Subject = "APK";
                        mailFunction.SendEmail();
                        return Json(true);

                    }
                    catch (Exception er)
                    {
                        _logger.LogError("APK", er);
                    }
                }
            }
            return Json(false);
        }

    }
}
