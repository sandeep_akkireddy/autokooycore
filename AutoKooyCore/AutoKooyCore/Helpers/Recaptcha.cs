﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoKooyCore
{
    public class Recaptcha
    {
        public string recaptchaPublickey { get; set; }
        public string recaptchaPrivatekey { get; set; }
    }
}
